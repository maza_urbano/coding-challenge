$(document).ready(function() {
	captura();
	function captura(){
		swal({
		  title: "Captura de datos",
		  text: "Ingresa el número de casos de prueba:",
		  type: "input",
		  closeOnConfirm: false,
		  animation: "slide-from-top",
		  inputPlaceholder: "Ingresa el número de casos de prueba"
		},
		function(inputValue){
			if (inputValue === false) return false;	  
			if (inputValue === "") {
		    	swal.showInputError("Necesitas ingresar un valor");
		    	return false
		  	}
			if (inputValue.match(/(^[1-9]{1}$|^[1-4]{1}[0-9]{1}$|^50$)/gm) == null){
				swal.showInputError("Escribe solo un valor entre 1 y 50");
		    	return false
			}
		  	$('#tabla_entrada > tbody:last-child').append('<tr id="n_i"><td>' + inputValue + '</td></tr>');
		  	var total = 1;
		  	DatosIteracion(inputValue, total);
		});
	}
	function DatosIteracion(inputValue, total){
		swal({
		  title: "Iteracion " + total,
		  text: "Ingresa la longitud de la matriz N, un espacio en blanco y el número de operaciones M",
		  type: "input",
		  closeOnConfirm: false,
		  animation: "slide-from-top",
		  inputPlaceholder: "N M"
		},
		function(inputValue2){
			if (inputValue2 === false) return false;	  
			if (inputValue2 === "") {
		    	swal.showInputError("Necesitas ingresar un valor");
		    	return false
		  	}
			if (inputValue2.match(/(^[1-9]{1}[ t]{1}[1-9]{1}$|^[1-9]{1}[0-9]{1}[ t]{1}[1-9]{1}$|^100[ t]{1}[1-9]{1}$|^[1-9]{1}[ t]{1}[1-9]{1}[0-9]{1}$|^[1-9]{1}[0-9]{1}[ t]{1}[1-9]{1}[0-9]{1}$|^100[ t]{1}[1-9]{1}[0-9]{1}$|^[1-9]{1}[ t]{1}[1-9]{1}[0-9]{1}[0-9]{1}$|^[1-9]{1}[0-9]{1}[ t]{1}[1-9]{1}[0-9]{1}[0-9]{1}$|^100[ t]{1}[1-9]{1}[0-9]{1}[0-9]{1}$|^100[ t]1000$|^[1-9]{1}[ t]1000|^[1-9]{1}[0-9]{1}[ t]1000)/) == null){
				swal.showInputError("Cadena ingresada erronea, o el valor de N 1-100 y M 1-1000");
	    		return false
			}
		  	$('#tabla_entrada > tbody:last-child').append('<tr id="n_m"><td>' + inputValue2 + '</td></tr>');
		  	data = inputValue2.split(" ");
		  	n = data[0];
		  	m = data[1];
		  	total = parseInt(total);
		  	inputValue = parseInt(inputValue);
		  	total = total + 1;
		  	if(total <= inputValue){
		  		DatosOPeracion(inputValue, total, m, 1, n);
	  		}
	  		if(total > inputValue){
	  			DatosOPeracion(inputValue, total, m, 1, n);
	  		}	
		});	
	}
	
	function DatosOPeracion(inputValue, total, Operaciones, totalO, N){
		swal({
		  title: "Operacion " + totalO + " de la Iteracion " + (parseInt(total) - 1),
		  text: "Operación: QUERY x1 y1 z1 x2 y2 z2 o UPDATE x y z N",
		  type: "input",
		  closeOnConfirm: false,
		  animation: "slide-from-top",
		  inputPlaceholder: "QUERY x1 y1 z1 x2 y2 z2 o UPDATE x y z N"
		},		
  		function(inputValue3){
  			if (inputValue3 === false) return false;	  
			if (inputValue3 === "") {
		    	swal.showInputError("Necesitas ingresar un valor");
		    	return false
		  	}
  			if (inputValue3.match(/(^QUERY[ t]{1}([1-9]{1}|[1-9]{1}[0-9]{1}|100)[ t]{1}([1-9]{1}|[1-9]{1}[0-9]{1}|100)[ t]{1}([1-9]{1}|[1-9]{1}[0-9]{1}|100)[ t]{1}([1-9]{1}|[1-9]{1}[0-9]{1}|100)[ t]{1}([1-9]{1}|[1-9]{1}[0-9]{1}|100)[ t]{1}([1-9]{1}|[1-9]{1}[0-9]{1}|100)$|^UPDATE[ t]{1}([1-9]{1}|[1-9]{1}[0-9]{1}|100)[ t]{1}([1-9]{1}|[1-9]{1}[0-9]{1}|100)[ t]{1}([1-9]{1}|[1-9]{1}[0-9]{1}|100)[ t]{1}[0-9-()]{1,10}$$)/) == null){
  				swal.showInputError("La cadena ingresada es erronea, verifica la sintaxis");
	    		return false
  			}
		  	data = inputValue3.split(" ");
		  	if (data[0] == 'QUERY' && (data[1] > n || data[2] > n || data[3] > n || data[4] > n || data[5] > n || data[6] > n)){
		  		swal.showInputError("El valor para x, y o z no puede ser mayor a " + n);
		    	return false
		  	}
		  	if (data[0] == 'QUERY' && (data[1] > data[4])){
		  		swal.showInputError("El valor para x1, no puede ser mayor a x2");
		    	return false
		  	}
		  	if (data[0] == 'QUERY' && (data[2] > data[5])){
		  		swal.showInputError("El valor para y1, no puede ser mayor a y2");
		    	return false
		  	}
		  	if (data[0] == 'QUERY' && (data[3] > data[6])){
		  		swal.showInputError("El valor para z1, no puede ser mayor a z2");
		    	return false
		  	}
		  	if (data[0] == 'UPDATE' && (data[1] > n || data[2] > n || data[3] > n)){
		  		swal.showInputError("El valor para x, y o z no puede ser mayor a " + n);
		    	return false
		  	}
		  	$('#tabla_entrada > tbody:last-child').append('<tr id="operacion"><td>' + inputValue3 + '</td></tr>');
		  	totalO = parseInt(totalO);
		  	totalO =  totalO + 1
		  	if (totalO <= Operaciones){
		  		DatosOPeracion(inputValue, total, Operaciones, totalO);
		  	}
		  	else{
			  	if(total <= inputValue){
			  		DatosIteracion(inputValue, total);
		  		}else{
		  			 swal("Se termino la captura", "");
		  		}
			}		  		
  		});
	}
	$("#nueva").click(function(){
		$('#tabla_entrada tbody > tr').remove();
		$('#tabla_salida tbody > tr').remove();
		captura();
		
	});
});

