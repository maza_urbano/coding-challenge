<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

Route::post('/crea_cubo', 'CuboController@crear_cubo');
Route::post('/actualiza_cubo', 'CuboController@actualiza_cubo');
Route::get('/muestra_cubo', 'CuboController@muestra_cubo');
Route::get('/reduce_cubo', 'CuboController@reduce_cubo');
Route::get('/suma_cubo', 'CuboController@suma_cubo');
Route::post('/resultado_cubo', 'CuboController@resultado_cubo');