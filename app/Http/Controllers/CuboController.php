<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Cache;

class CuboController extends Controller
{
	public function crear_cubo(Request $request)
	{
		$data = $request->all();
		$n = $data['n'];
		$cubo = array_fill(1, $n, array_fill(1, $n, array_fill(1, $n, 0))); 
		Cache::put('cubo', $cubo, 10);
		return \Response::json(array('success'=> true, 'msg'=>'Cubo creado'));
	}
	
	public function actualiza_cubo(Request $request)
	{
		$data = $request->all();
		$x = $data['x'];
		$y = $data['y'];
		$z = $data['z'];
		$W = $data['W'];
		$cubo = Cache::get('cubo');
		$cubo[$x][$y][$z]= $W;
		Cache::put('cubo', $cubo, 10);
		return \Response::json(array('success'=> true, 'msg'=>'Cubo actualizado'));
	}
	
	public function muestra_cubo()
	{
		$cubo = Cache::get('cubo');
		print_r($cubo);
		
	}
	
	public function resultado_cubo(Request $request){
		$data = $request->all();	
		$x1 = $data['x1'];
		$y1 = $data['y1'];
		$z1 = $data['z1'];
		$x2 = $data['x2'];
		$y2 = $data['y2'];
		$z2 = $data['z2'];
		$cubo = Cache::get('cubo');
		$cubo = $this->reduce_cubo($cubo, $x1, $y1, $z1, $x2, $y2, $z2);
		$suma = $this->suma_cubo($cubo);
		return \Response::json(array('success'=> true, 'msg'=>$suma));
	}
	
	public function reduce_cubo($cubo, $x1, $y1, $z1, $x2, $y2, $z2)
	{
		$cubo = Cache::get('cubo');
		$cuboX = array_slice($cubo, $x1-1, $x2, true);
		$x = $x1;
		foreach ($cuboX as $sub_array) {
		    $cuboXY[$x] = array_slice($sub_array, $y1-1, $y2, true);
			$x = $x+1;
		}
		$x = $x1;
		foreach ($cuboXY as $sub_array) {
			$y = $y1;	
			foreach ($sub_array as $sub_arrayB) {
				$cuboXYZ[$x][$y] = array_slice($sub_arrayB, $z1-1, $z2, true);
				$y = $y+1;
			}
			$x = $x+1;
		}
		return $cuboXYZ;
	}
	
	public function suma_cubo($cuboXYZ)
	{
		$suma = 0;
		foreach ($cuboXYZ as $cuboYZ) {
			foreach ($cuboYZ as $cuboZ) {
				foreach($cuboZ as $valoresZ){
					$suma = $suma + $valoresZ;
				}
			}
		}
		return $suma;
	}
}
