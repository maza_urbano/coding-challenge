$(document).ready(function() {
	$("#procesar").click(function(){	
		window.niteracion = 0;
		window.linea = 1;
		window.n = 0;
		window.m = 0;	
		$('#tabla_entrada tr').each(function() {
			switch (linea){
				case 2:
	   				data = $(this).find("td:first").html();
			   		crea_cubo(data);
			   		break;
		   		case window.niteracion:
		   			data = $(this).find("td:first").html();
			   		crea_cubo(data);
			   		break;
	   			default:
	   				data = $(this).find("td:first").html();
	   				operacion(data);
	   				break;			
			}
			linea = linea + 1;
		});
	});
	function crea_cubo(data){
		data = data.split(" ");
   		window.n = data[0];
   		window.m = data[1];
   		window.niteracion = parseInt(window.linea) + parseInt(window.m) + 1;
   		var token = $('#token').val();
   		$.ajax({
	        type: "POST",
	        url: '/crea_cubo',
	        data: {n: window.n, _token: token},
	        async: false,
	        dataType : 'json',
	        beforeSend: function() {
	            swal({
					title: 'Enviando Datos...',
					html: true,
					showConfirmButton:false
				});
	        },
	        success: function(response) {
	        	console.log(response.msg);
	        	swal.close();
	        },
	        error: function(request, status, error){
	        	swal({title: "¡Error!",   text: "Por favor intente de nuevo",   type: "error",   confirmButtonText: "Aceptar" });
	        }
	    });
	}
	function operacion(data){
		data = data.split(" ");
		var token = $('#token').val();
		var url = '';
		if(data[0] == 'UPDATE'){
			url = '/actualiza_cubo';
			var datos = {x: data[1], y: data[2], z: data[3], W: data[4], _token: token}
		}
		if(data[0] == 'QUERY'){
			url = '/resultado_cubo';
			var datos = {x1: data[1], y1: data[2], z1: data[3], x2: data[4], y2: data[5], z2: data[6], _token: token}
		}
   		if(url != ''){
			$.ajax({
		        type: "POST",
		        url: url,
		        data: datos,
		        async: false,
		        dataType : 'json',
		        beforeSend: function() {
		            swal({
						title: 'Enviando Datos...',
						html: true,
						showConfirmButton:false
					});
		        },
		        success: function(response) {
		        	console.log(response.msg);
		        	if (data[0] == 'QUERY'){
		        		$('#tabla_salida > tbody:last-child').append('<tr><td>' + response.msg + '</td></tr>');
		        	}
		        	swal.close();
		        },
		        error: function(request, status, error){
		        	swal({title: "¡Error!",   text: "Por favor intente de nuevo",   type: "error",   confirmButtonText: "Aceptar" });
		        }
		    });
	    }
	}
});

