<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Codding Challenge</title>
	
		{!! Html::style('/css/app.css')!!}
		{!! Html::style('css/sweetalert.css')!!}
		{!! Html::style('themify-icons/themify-icons.min.css')!!}
		{!! Html::script('js/jquery.js')!!}
		{!! Html::script('js/jquery-ui.js')!!}
		{!! Html::script('js/sweetalert.min.js')!!}
		{!! Html::script('js/captura.js')!!}
		{!! Html::script('js/cubo.js')!!}
	
		{!! Html::script('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js')!!}
	</head>
	<body>
		<br>
		<div class="container" id="container">
			<div class="col-md-12"><center><h3>Cube Summation</h3></center></div>
			<input type="hidden" id="token" value="<?php echo csrf_token(); ?>"/>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">Datos de entrada</div>
					<div class="panel-body">
						<table id="tabla_entrada">
						  <tbody>
						  </tbody>
						</table>
						
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">Resultado</div>
					<div class="panel-body">
						<table id="tabla_salida">
						  <tbody>
						  </tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<button type="button" class="btn btn-primary" id="procesar">
		  			Procesar Datos
				</button>
				<button type="button" class="btn btn-primary" id="nueva">
		  			Nueva Suma
				</button>
			</div>
		</div>
	</body>
</html>
